import { Component } from "react";

class Lock extends Component {
  render() {
    const { turn, locked } = this.props;
    return <button onClick={turn}> {locked ? "Destravar" : "Travar"}</button>;
  }
}
export default Lock;
