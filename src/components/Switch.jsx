import { Component } from "react";

class Switch extends Component {
  render() {
    const { turn, show, locked } = this.props;
    return (
      <button disabled={locked} onClick={turn}>
        {show ? "Esconder" : "Mostrar"}
      </button>
    );
  }
}
export default Switch;
