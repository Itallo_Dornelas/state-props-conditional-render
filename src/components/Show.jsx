import { Component } from "react";

class Show extends Component {
  render() {
    const { children, shows } = this.props;
    return <div>{shows ? <div>{children}</div> : <h2>Sumiu!!</h2>}</div>;
  }
}
export default Show;
