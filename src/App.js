import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import Switch from "./components/Switch";
import Show from "./components/Show";
import Lock from "./components/Lock";
import "./components/style.css";

class App extends Component {
  state = {
    showLogo: true,
    locked: false,
  };
  handleShowLogo = () => {
    const { showLogo } = this.state;
    this.setState({
      showLogo: !showLogo,
    });
  };
  handleLocked = () => {
    const { locked } = this.state;
    this.setState({
      locked: !locked,
    });
  };
  render() {
    const { showLogo, locked } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <Show shows={showLogo}>
            <img src={logo} className="App-logo" alt="logo" />
          </Show>

          <Switch locked={locked} show={showLogo} turn={this.handleShowLogo} />
          <Lock locked={locked} turn={this.handleLocked} />
        </header>
      </div>
    );
  }
}

export default App;
